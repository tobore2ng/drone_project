/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.model;

/**
 *
 * Drone class is model for the drone object
 */
public class Drone {
    
    private String serial;
    private String model;
    private double weight;
    private int battery;
    private String state;
    private String  medicationCode;

    public Drone(String serial,String model,double weight,int battery,String state){
    this.serial=serial;
    this.model=model;
    this.weight=weight;
    this.battery=battery;
    this.state=state;  
    }

    public Drone() {
        
    }
    /**
     * @return the serial
     */
    public String getSerial() {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the drone model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @param weight the drone weight to set
     */
    public void setWeight(Double weight) {
        this.weight = weight;
    }

    /**
     * @return the battery
     */
    public int getBattery() {
        return battery;
    }

    /**
     * @param battery the drone battery to set
     */
    public void setBattery(int battery) {
        this.battery = battery;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the drone state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the medicationCode
     */
    public String getMedicationCode() {
        return medicationCode;
    }

    /**
     * @param medicationCode the medicationCode to set for drone when loading medication on drone
     */
    public void setMedicationCode(String medicationCode) {
        this.medicationCode = medicationCode;
    }
}
