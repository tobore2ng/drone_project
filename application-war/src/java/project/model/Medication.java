/*
 * 
 *  
 *  
 */
package project.model;

/**
 *Medication class is a model that hold information about medication that will be loaded on a given drone
 */
public class Medication {
 
    private String name;
    private double weight;
    private String code;
    private String image;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the medication name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @param weight the medication weight to set
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the medication code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the medication image to set
     */
    public void setImage(String image) {
        this.image = image;
    }
}
