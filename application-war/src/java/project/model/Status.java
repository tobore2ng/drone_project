/*
 *  Status class holds two array strings
 *  possibleState is an array with the possble drone states
 *  possibleModel is the model version of the drone
 */
package project.model;
 
public class Status {
    //All possible state of drone instances
    public static String possibleState[] ={"IDLE", "LOADING", "LOADED", "DELIVERING", "DELIVERED", "RETURNING"}; 
    //All possible model of drone instances
public static String possibleModel[] ={"Lightweight", "Middleweight", "Cruiserweight", "Heavyweight"}; 
}
