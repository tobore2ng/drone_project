/*
 *  
 *  
 *  
 */
package project.controller;

import java.util.regex.Pattern;
import project.model.Drone;
import project.model.Medication;

/**
 *
 */
public class Activity {

    public String checkDrone() {
        return "";
    }

    /** check/validate drone information before inserting into database
     * 
     * @param d drone instance
     * @return status of the validation
     */
    public String checkDrone(Drone d) {
        if (DataStore.getInstance().checkDroneSerialNumber(d.getSerial())) {
            return "Drone serial number already exist!";
        } else if (d.getSerial().length() > 100) {
            return "Drone serial number should not exceed 100!";
        } else if (d.getSerial().trim().length() == 0) {
            return "Drone serial number missing";
        } else if (d.getModel().trim().length() == 0) {
            return "Drone model value missing";
        } else if (d.getWeight() > 500) {
            return "Drone weight exceeds maximum limit";
        } else if (d.getWeight() <= 0) {
            return "Drone weight is invalid";
        } else {
            return "success";
        }
    }

    /**check/validate satus of the medication before registration and loading on drone
     * 
     * @param med medication instance
     * @return status of the validation
     */
    public String checkMedication(Medication med) {
        Pattern medicationNamePattern = Pattern.compile("^[a-zA-Z_\\-]*$");
        Pattern medicationCodePattern = Pattern.compile("^[A-Z0-9_]*$");
        if (med.getName().trim().length() == 0) {
            return "No value found for medication name";
        } else if (med.getCode().trim().length() == 0) {
            return "No value found for medication code";
        } else if (med.getImage().trim().length() == 0) {
            return "No image found for medication";
        } else if (med.getWeight() == 0) {
            return "Medication weight cannot be zero";
        } else if (!medicationNamePattern.matcher(med.getName()).find()) {
            return "Wrong value for medication name";
        } else if (!medicationCodePattern.matcher(med.getName()).find()) {
            return "Wrong value for medication code";
        } else {
            return "success";
        }
    }
    /**
     * Check the condition of drone to load the medication
     * @param drone instance
     * @param med medication instance
     * @return load medication
     */
    public String checkLoadingDrone(Drone drone,Medication med){
     
        if(med.getWeight()>drone.getWeight()){
          return "Drone cannot be loaded with medication, weight exceed limit for the drone";
        } else if(drone.getBattery()< 25){
         return "Drone cannot be loaded, battery is below 25% ";
        }else if(!drone.getState().equalsIgnoreCase("IDLE")){
         return "Drone cannot be loaded, state is not idle for loading ";
        }else{
         return "success";
        }
        
            
    }
}
