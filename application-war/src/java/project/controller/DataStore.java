package project.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import project.model.Drone;
import project.model.Medication;
import project.model.Status;

/**
 *  DataStore class that provides access to drone and medication data.
 * assume this class accesses a database.
 */
public class DataStore {

	//Drone database:Map of serial to Drone instances.
	private Map<String, Drone> droneMap = new HashMap<String, Drone>();
        
        private ConcurrentHashMap<String,String> droneBattery  =new ConcurrentHashMap<String,String>();
        
        //Medication database:Map of code to Medication instances.
        private Map<String, Medication> medicationMap = new HashMap<String, Medication>();
        
        private Random random=new Random(); 
	
       //this class is a singleton and should not be instantiated directly!
	private static DataStore instance = new DataStore();
	
        
        public static DataStore getInstance(){
		return instance;
	}

	//private constructor so people know to use the getInstance() function instead
	private DataStore(){
		//10 fleet of drone data, randomly initialised
               try { 
                 byte[] array;
                 Drone drone;
                for(int i=0;i<10;i++){
                    array = new byte[20];
                    random.nextBytes(array);
                  String serial=genrateSerialString(10);// generate random serial ID
                  while(true){
                    if(droneMap.containsKey(serial)){
                          serial=genrateSerialString(10);
                    }else{
                      break;
                    }
                        
                  }
                  String model=Status.possibleModel[random.nextInt(Status.possibleModel.length)];
                  int weight = (random.nextInt(3)+1) *100;
                  int battery = random.nextInt(50) *2;
                  String state=Status.possibleState[ i% Status.possibleState.length]; 
                  drone=new Drone(serial,model,weight,battery,state);
                  droneMap.put(serial, drone);
                } 
                
                  //audit event log for drone battery
                  Thread audit=new Thread(new Runnable(){
                   public void run(){
                       try {
                           for(String key:droneMap.keySet()){
                              if(droneBattery.containsKey(key)){
                                droneBattery.put(key, droneBattery.get(key)+"\n"+droneMap.get(key).getBattery()+"%");
                              }else{
                              droneBattery.put(key, droneMap.get(key).getBattery()+"%");
                              }
                                  
                           }
                           this.wait(120000);// wait for 3 min to create event log
                           
                       } catch (Exception e) {
                       }
                   }
                  });
                  audit.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
	} 
        
        /**generate random string content to form drone serial ID
         * 
         * @param len
         * @return 
         */
        private String genrateSerialString(int len) {
        char[] buf=new char[len];
        String upper = "ABCDEFGHIJKLMNPQRSTUVWXYZ"; 
        String digits = "123456789";
        String alphanum = upper + digits;
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = alphanum.charAt(random.nextInt(alphanum.length()));
        return new String(buf);
    }
        
        /**Get a particular drone from the database using serial ID
         * 
         * @param serial drone instance
         * @return Drone object
         */
	public Drone getDrone(String serial) {
            if(droneMap.containsKey(serial))
		return droneMap.get(serial);
            else
                return null;
	}
        
        /**check if drone already exist in database
         * 
         * @param serialNumber drone
         * @return boolean if drone already exist
         */
        public boolean checkDroneSerialNumber(String serialNumber){
         if(droneMap.containsKey(serialNumber)){
           return true;
         }else{
            return false;
         }
        }

        /**save drone information
         * 
         * @param drone instance
         * @return status of the registration
         */
	public String putDrone(Drone drone) {  
		droneMap.put(drone.getSerial(), drone); 
                return "Drone information registered successfully!";
	} 
        
        /**Get all registered drones
         * 
         * @return all registered drones
         */
        public Map<String, Drone> getAllRegisteredDrone(){
           return droneMap;
        }
         
        /**Load drone with medication
         * 
         * @param drone  instance
         * @param medication instance
         * @return status of the load process
         */
        public String loadDroneWithMedication(Drone drone,Medication medication){
          try{
               drone.setState(Status.possibleState[1]);// change the drone status to loading
               droneMap.replace(drone.getSerial(), drone);
               drone.setMedicationCode(medication.getCode());
               medicationMap.put(medication.getCode(), medication);
               drone.setState(Status.possibleState[2]);// change the drone status to loaded
               droneMap.replace(drone.getSerial(), drone);
               return "success"; 
          }catch(Exception e){
              return "error"; 
          }
        }
}

