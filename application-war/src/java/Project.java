/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;

import org.json.JSONObject;
import project.controller.Activity;
import project.controller.DataStore;
import project.model.Drone;
import project.model.Medication;
import project.model.Status;

/**
 *
 * @author pp
 */
public class Project extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            DataStore dataStore = DataStore.getInstance(); // get datastore instance
            String requestUrl = request.getRequestURI(); 
            JSONObject jsonObject = new JSONObject(); // json object to build response
            Map<String, Drone> allDrones = dataStore.getAllRegisteredDrone();
            if (requestUrl.contains("alldrone")) {  // handle and view all registered drones
                for (String key : allDrones.keySet()) {
                    JSONObject jobj = new JSONObject(); // temp json object
                    Drone drone = allDrones.get(key); 
                        jobj.put("serial:", drone.getSerial());
                        jobj.put("model:", drone.getModel());
                        jobj.put("weight:", drone.getWeight() + "gr");
                        jobj.put("battery:", drone.getBattery() + "%");
                        jobj.put("state:", drone.getState());
                        jsonObject.put(drone.getSerial(), jobj); 
                }
                response.getOutputStream().print(jsonObject.toString());

            } else if (requestUrl.contains("availabledrone")) {    // handle and view all registered drones avaialble for loading
                for (String key : allDrones.keySet()) {
                    Drone drone = allDrones.get(key);
                    if (drone.getState().equalsIgnoreCase("IDLE")) { //drone with idle status are avaialble drones for loading medication
                            JSONObject jobj = new JSONObject();
                            jobj.put("serial:", drone.getSerial());
                            jobj.put("model:", drone.getModel());
                            jobj.put("weight:", drone.getWeight() + "gr");
                            jobj.put("battery:", drone.getBattery() + "%");
                            jobj.put("state:", drone.getState());
                            jsonObject.put(drone.getSerial(), jobj); 
                    }
                }
                response.getOutputStream().print(jsonObject.toString());

            } else if (requestUrl.contains("checkdronesbattery")) {  // handle and view  drones battery
                for (String key : allDrones.keySet()) {
                    Drone drone = allDrones.get(key); 
                        JSONObject jobj = new JSONObject();
                        jobj.put("serial:", drone.getSerial());
                        jobj.put("battery:", drone.getBattery() + "%");
                        jsonObject.put(drone.getSerial(), jobj); 
                }
                response.getOutputStream().print(jsonObject.toString());
            } else {
                response.getOutputStream().println("{}");
            }
        } catch (Exception ex) {
            response.getOutputStream().println("{}");
            Logger.getLogger(Project.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        try {
            Activity activity = new Activity();
            DataStore dataStore = DataStore.getInstance();
            String jsonBody = new BufferedReader(new InputStreamReader(request.getInputStream())).lines().collect(
                    Collectors.joining("\n")); // get json from user input
            JSONObject jObj = new JSONObject(jsonBody);
            JSONObject jsonObject = new JSONObject();
            if (jObj.get("action").toString().equalsIgnoreCase("registerdrone")) { // handle post request to register drone

                Drone drone = new Drone();
                drone.setSerial(jObj.get("serial").toString());
                drone.setModel(jObj.get("model").toString());
                drone.setWeight(Double.parseDouble(jObj.get("weight").toString()));
                drone.setBattery(Integer.parseInt(jObj.get("battery").toString()));
                drone.setState(Status.possibleState[0]);
                String returnValue = activity.checkDrone(drone);//check stauts of drone before registration
                if (returnValue.equalsIgnoreCase("success")) {
                    jsonObject.put("status", "Success");
                    jsonObject.put("message", dataStore.putDrone(drone));
                    response.getOutputStream().println(jsonObject.toString());

                } else {
                    jsonObject.put("status", "Error");
                    jsonObject.put("message", returnValue);
                    response.getOutputStream().println(jsonObject.toString());

                }

            } else if (jObj.get("action").toString().equalsIgnoreCase("loadedstate")) { // handle post request to view loaded state for each drone
                String loadState = jObj.get("serial").toString();
                Drone drone = dataStore.getDrone(loadState);
                if (drone != null) {
                    jsonObject.put("state", drone.getState());
                    jsonObject.put("status", "success");
                    response.getOutputStream().println(jsonObject.toString());
                } else {
                    jsonObject.put("message", "Drone is null");
                    jsonObject.put("status", "error");
                    response.getOutputStream().println(jsonObject.toString());
                }

            } else if (jObj.get("action").toString().equalsIgnoreCase("getbattery")) { // handle post request to  view battery for each drone
                String srial = jObj.get("serial").toString();
                Drone drone = dataStore.getDrone(srial);
                if (drone != null) {
                    jsonObject.put("battery", drone.getBattery() + "%");
                    jsonObject.put("status", "success");
                    response.getOutputStream().println(jsonObject.toString());
                } else {
                    jsonObject.put("message", "Drone is null");
                    jsonObject.put("status", "error");
                    response.getOutputStream().println(jsonObject.toString());
                }

            } else if (jObj.get("action").toString().equalsIgnoreCase("loadmedicationdrone")) { // handle post request to load medication on drone

                Medication medication = new Medication();
                medication.setName(jObj.get("name").toString());
                medication.setWeight(Double.parseDouble(jObj.get("weight").toString()));
                medication.setCode(jObj.get("code").toString());
                medication.setImage(jObj.get("image").toString());//file for image, assume its a image path
                String droneSerial = jObj.get("serial").toString(); // serial code for drone;
                String returnValue = activity.checkMedication(medication);//check/validate medication  status
                if (returnValue.equalsIgnoreCase("success")) {
                    Drone drone = dataStore.getDrone(droneSerial);
                    if (drone == null) {
                        jsonObject.put("status", "error");
                        jsonObject.put("message", "Wrong drone serial ID value");
                        response.getOutputStream().println(jsonObject.toString());
                    } else {
                        String returnResult = activity.checkLoadingDrone(drone, medication);
                        if (returnResult.equalsIgnoreCase("success")) {
                            returnResult = dataStore.loadDroneWithMedication(drone, medication); // load medication on drone

                            if (returnResult.equalsIgnoreCase("success")) {

                                jsonObject.put("status", "success");
                                jsonObject.put("message", "Medication loaded on drone successfully");
                            } else {
                                jsonObject.put("status", "error");
                                jsonObject.put("message", "Internal error occured while loading medication on drone");
                            }
                            response.getOutputStream().println(jsonObject.toString());

                        } else {
                            jsonObject.put("status", "warning");
                            jsonObject.put("message", returnResult);
                            response.getOutputStream().println(jsonObject.toString());

                        }
                    }
                } else {
                    jsonObject.put("status", "error");
                    jsonObject.put("message", returnValue);
                    response.getOutputStream().println(jsonObject.toString());

                }

            } else {
                response.getOutputStream().println("{}");
            }
        } catch (Exception ex) {
            response.getOutputStream().println("{}");
            Logger.getLogger(Project.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
